/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */


export class World {
  constructor() {
    this.households = {};
    this.killsPowerPlant = [];
    this.id = 1;
  }

  createPowerPlant() {
    return (this.id += 1);
  }

  createHousehold() {
    this.id += 1;

    if (!this.households[this.id]) {
      this.households[this.id] = { powerPlants: [] }
    }

    return this.id;
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    if (!this.killsPowerPlant.includes(powerPlant)) {
      this.households[household].powerPlants.push(powerPlant);
    }
  }

  connectHouseholdToHousehold(household1, household2) {
    const sumPower = new Set();
    this.households[household1].powerPlants.forEach(powerPlant =>
      sumPower.add(powerPlant));
    this.households[household2].powerPlants.forEach(powerPlant =>
      sumPower.add(powerPlant));

    const allPower = Array.from(sumPower);
    this.households[household1].powerPlants = allPower;
    this.households[household2].powerPlants = allPower;
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    const indexPowerPlant = this.households[household].powerPlants.indexOf(powerPlant);
    this.households[household].powerPlants.splice(indexPowerPlant, 1);
  }

  killPowerPlant(powerPlant) {
    this.killsPowerPlant.push(powerPlant);

    Object.keys(this.households).forEach((household) => {
      const indexPowerPlant = this.households[household].powerPlants.indexOf(powerPlant);

      if (indexPowerPlant !== -1) {
        this.households[household].powerPlants.splice(indexPowerPlant, 1);
      }
    })
  }

  repairPowerPlant(powerPlant) {
    const indexPowerPlant = this.killsPowerPlant.indexOf(powerPlant);
    this.killsPowerPlant.splice(indexPowerPlant, 1);

    Object.keys(this.households).forEach((household) => {
      if (!this.households[household].powerPlants.includes(powerPlant)) {
        this.households[household].powerPlants.push(powerPlant);
      }
    })
  }

  householdHasEletricity(household) {
    if (this.households[household].powerPlants.length) {
      return true;
    }
    return false;
  }
}
